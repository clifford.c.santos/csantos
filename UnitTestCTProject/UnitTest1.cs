﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CSantosCTProject.Parcel;

namespace UnitTestCTProject
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMediumParcel()
		{
			string expectedCost = "$80.00";
			string expectedCategory = "Medium Parcel";

			var parcel = new RejectParcel();
			var heavyParcel = new HeavyParcel();
			var smallParcel = new SmallParcel();
			var mediumParcel = new MediumParcel();
			var largeParcel = new LargeParcel();

			parcel.SetNext(heavyParcel).SetNext(smallParcel).SetNext(mediumParcel).SetNext(largeParcel);

			var userInputModel = new UserInputModel();
			userInputModel.Weight = 10;
			userInputModel.Height = 20;
			userInputModel.Width = 5;
			userInputModel.Depth = 20;

			var parcelValueModel = parcel.ComputeCost(userInputModel);

			Assert.AreEqual(expectedCategory, parcelValueModel.Category, "Parcel Category is correct");
			Assert.AreEqual(expectedCost, parcelValueModel.Cost, "Parcel Cost is correct");
		}

		[TestMethod]
		public void TestHeavyParcel()
		{
			string expectedCost = "$330.00";
			string expectedCategory = "Heavy Parcel";

			var parcel = new RejectParcel();
			var heavyParcel = new HeavyParcel();
			var smallParcel = new SmallParcel();
			var mediumParcel = new MediumParcel();
			var largeParcel = new LargeParcel();

			parcel.SetNext(heavyParcel).SetNext(smallParcel).SetNext(mediumParcel).SetNext(largeParcel);

			var userInputModel = new UserInputModel();
			userInputModel.Weight = 22;
			userInputModel.Height = 5;
			userInputModel.Width = 5;
			userInputModel.Depth = 5;

			var parcelValueModel = parcel.ComputeCost(userInputModel);

			Assert.AreEqual(expectedCategory, parcelValueModel.Category, "Parcel Category is correct");
			Assert.AreEqual(expectedCost, parcelValueModel.Cost, "Parcel Cost is correct");
		}

		[TestMethod]
		public void TestSmallParcel()
		{
			string expectedCost = "$18.00";
			string expectedCategory = "Small Parcel";

			var parcel = new RejectParcel();
			var heavyParcel = new HeavyParcel();
			var smallParcel = new SmallParcel();
			var mediumParcel = new MediumParcel();
			var largeParcel = new LargeParcel();

			parcel.SetNext(heavyParcel).SetNext(smallParcel).SetNext(mediumParcel).SetNext(largeParcel);

			var userInputModel = new UserInputModel();
			userInputModel.Weight = 2;
			userInputModel.Height = 3;
			userInputModel.Width = 10;
			userInputModel.Depth = 12;

			var parcelValueModel = parcel.ComputeCost(userInputModel);

			Assert.AreEqual(expectedCategory, parcelValueModel.Category, "Parcel Category is correct");
			Assert.AreEqual(expectedCost, parcelValueModel.Cost, "Parcel Cost is correct");
		}

		[TestMethod]
		public void TestRejectedParcel()
		{
			string expectedCost = "N/A";
			string expectedCategory = "Rejected";

			var parcel = new RejectParcel();
			var heavyParcel = new HeavyParcel();
			var smallParcel = new SmallParcel();
			var mediumParcel = new MediumParcel();
			var largeParcel = new LargeParcel();

			parcel.SetNext(heavyParcel).SetNext(smallParcel).SetNext(mediumParcel).SetNext(largeParcel);

			var userInputModel = new UserInputModel();
			userInputModel.Weight = 110;
			userInputModel.Height = 20;
			userInputModel.Width = 55;
			userInputModel.Depth = 120;

			var parcelValueModel = parcel.ComputeCost(userInputModel);

			Assert.AreEqual(expectedCategory, parcelValueModel.Category, "Parcel Category is correct");
			Assert.AreEqual(expectedCost, parcelValueModel.Cost, "Parcel Cost is correct");
		}

		[TestMethod]
		public void TestLargeParcel()
		{
			string expectedCost = "$120.00";
			string expectedCategory = "Large Parcel";

			var parcel = new RejectParcel();
			var heavyParcel = new HeavyParcel();
			var smallParcel = new SmallParcel();
			var mediumParcel = new MediumParcel();
			var largeParcel = new LargeParcel();

			parcel.SetNext(heavyParcel).SetNext(smallParcel).SetNext(mediumParcel).SetNext(largeParcel);

			var userInputModel = new UserInputModel();
			userInputModel.Weight = 10;
			userInputModel.Height = 40;
			userInputModel.Width = 5;
			userInputModel.Depth = 20;

			var parcelValueModel = parcel.ComputeCost(userInputModel);

			Assert.AreEqual(expectedCategory, parcelValueModel.Category, "Parcel Category is correct");
			Assert.AreEqual(expectedCost, parcelValueModel.Cost, "Parcel Cost is correct");
		}
	}
}
