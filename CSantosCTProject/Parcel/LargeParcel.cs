﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSantosCTProject.Parcel
{
	class LargeParcel : ParcelBase
	{
		public override ParcelValueModel ComputeCost(UserInputModel userInputModel)
		{
			parcelValueModel.Category = "Large Parcel";
			parcelValueModel.Cost = (0.03 * Volume.GetVolume(userInputModel.Height, userInputModel.Width, userInputModel.Depth)).ToString("C", GetCurrencyCulture());
			return parcelValueModel;
		}
	}
}
