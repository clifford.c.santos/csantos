﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSantosCTProject.Parcel
{
	class RejectParcel : ParcelBase
	{
		public override ParcelValueModel ComputeCost(UserInputModel userInputModel)
		{
			if (userInputModel.Weight > 50)
			{
				parcelValueModel.Category = "Rejected";
				parcelValueModel.Cost = "N/A";
				return parcelValueModel;
			}
			return base.ComputeCost(userInputModel);
		}
	}
}
