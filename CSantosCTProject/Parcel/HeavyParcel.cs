﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSantosCTProject.Parcel
{
	class HeavyParcel : ParcelBase
	{
		public override ParcelValueModel ComputeCost(UserInputModel userInputModel)
		{
			if (userInputModel.Weight > 10)
			{
				parcelValueModel.Category = "Heavy Parcel";
				parcelValueModel.Cost =  (15 * userInputModel.Weight).ToString("C", GetCurrencyCulture());
				return parcelValueModel;
			}

			return base.ComputeCost(userInputModel);
		}
	}
}
