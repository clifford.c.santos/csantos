﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSantosCTProject.Parcel
{
	public class UserInputModel
	{
		public int Weight { get; set; }
		public int Height { get; set; }
		public int Width { get; set; }
		public int Depth { get; set; }
	}
}
