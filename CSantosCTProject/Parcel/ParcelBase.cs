﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace CSantosCTProject.Parcel
{
	abstract class ParcelBase : IParcel
	{
		private IParcel parcel;
		protected ParcelValueModel parcelValueModel;

		protected ParcelBase()
		{
			parcelValueModel = new ParcelValueModel();
		}

		public virtual ParcelValueModel ComputeCost(UserInputModel userInputModel)
		{
			if (this.parcel != null)
			{
				return this.parcel.ComputeCost(userInputModel);
			}
			else
			{
				return null;
			}
		}

		public IParcel SetNext(IParcel parcel)
		{
			this.parcel = parcel;
			return parcel;
		}

		protected CultureInfo GetCurrencyCulture()
		{
			return new CultureInfo("en-US");
		}

	}
}
