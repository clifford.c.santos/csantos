﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSantosCTProject.Parcel
{
	public class Volume
	{
		public static int GetVolume(int height, int width, int depth)
		{
			return (height * width * depth);
		}
	}
}
