﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSantosCTProject.Parcel
{
	class SmallParcel : ParcelBase
	{
		public override ParcelValueModel ComputeCost(UserInputModel userInputModel)
		{
			int volume = Volume.GetVolume(userInputModel.Height, userInputModel.Width, userInputModel.Depth);
			if (volume < 1500)
			{
				parcelValueModel.Category = "Small Parcel";
				parcelValueModel.Cost = (0.05 *volume).ToString("C", GetCurrencyCulture());
				return parcelValueModel;
			}
			return base.ComputeCost(userInputModel);
		}
	}
}
