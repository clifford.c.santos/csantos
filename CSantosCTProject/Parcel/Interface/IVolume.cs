﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSantosCTProject.Parcel
{
	interface IVolume
	{
		int GetVolume(int height, int width, int depth);
	}
}
