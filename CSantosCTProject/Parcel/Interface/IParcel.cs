﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSantosCTProject.Parcel
{
	interface IParcel
	{
		IParcel SetNext(IParcel parcel);
		ParcelValueModel ComputeCost(UserInputModel userInputModel);
	}
}
