﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSantosCTProject.Parcel;

namespace CSantosCTProject
{
	class Program
	{
		static void Main(string[] args)
		{
			var userInputModel = new UserInputModel();
			userInputModel.Weight = GetValue("Enter Weight in kg: ", "Please input valid weight value.");
			userInputModel.Height = GetValue("Enter Height in cm: ", "Please input valid height value.");
			userInputModel.Width = GetValue("Enter Width in cm: ", "Please input valid width value.");
			userInputModel.Depth = GetValue("Enter Depth in cm: ", "Please input valid depth value.");

			var parcel = new RejectParcel();
			var heavyParcel = new HeavyParcel();
			var smallParcel = new SmallParcel();
			var mediumParcel = new MediumParcel();
			var largeParcel = new LargeParcel();

			parcel.SetNext(heavyParcel).SetNext(smallParcel).SetNext(mediumParcel).SetNext(largeParcel);

			var parcelValueModel = parcel.ComputeCost(userInputModel);

			Console.WriteLine();
			Console.WriteLine(string.Concat("Category: ", parcelValueModel.Category));
			Console.WriteLine(string.Concat("Cost: ", parcelValueModel.Cost));
			Console.ReadKey();
		}

		static int GetValue(string message, string errorMessage)
		{
			bool validState = false;
			int temp = 0;

			while (validState != true)
			{
				Console.Write(message);
				if (int.TryParse(Console.ReadLine(), out temp))
				{
					validState = true;
				}
				else
				{
					Console.WriteLine(errorMessage);
				}
			}
			return temp;
		}

	}
}
